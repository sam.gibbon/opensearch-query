import requests
import pandas as pd
import time
from helper import save_csv, RAW

start_overall_time = time.perf_counter() * 1000

# Define the URL, headers, and data
url = "https://logs.internalbp.com/_search?scroll=1m"
scroll_url = "https://logs.internalbp.com/_search/scroll"
headers = {'Content-Type': 'application/json'}

page_size = 10000
max_pages = 100
hits = []

json_request = {
    "size": page_size,
    "sort": [
        {
            "@timestamp": {
                "order": "desc"
            }
        }
    ],
    "query": {
        "bool": {
            "should": [
                {
                    "match_phrase": {
                        "msg": "starting export of",
                    }
                },
                {
                    "match_phrase": {
                        "msg": "completed export of",
                    }
                }
            ],
            "minimum_should_match": 1,
            "filter": {
                "range": {
                    "@timestamp": {
                        "gte": "2024-01-01T00:00:00.000Z",
                        "lte": "2024-11-31T23:59:59.999Z"
                    }
                }

            }
        }
    }
}

# Send the request and get the response
print('Making initial request')
start_time = time.perf_counter() * 1000
response = requests.post(url, headers=headers, json=json_request)
end_time = time.perf_counter() * 1000
print('Initial request complete in ' + str(round(end_time - start_time)) + 'ms')

# Convert the response to JSON
json_data = response.json()

scroll_id = json_data['_scroll_id']

# Access the "hits" array within the "hits" field
data = json_data['hits']['hits']

hits.extend(data)

page = 2
while len(data) == page_size and page <= max_pages:
    scroll_request = {
        "scroll": "1m",
        "scroll_id": scroll_id
    }

    # Send the scroll request and get the response
    print('Fetching page ' + str(page))
    start_time = time.perf_counter() * 1000
    response = requests.post(scroll_url, headers=headers, json=scroll_request)
    end_time = time.perf_counter() * 1000

    # Convert the response to JSON
    json_data = response.json()

    # Access the "hits" array within the "hits" field
    data = json_data['hits']['hits']
    if len(data) == 0:
        break

    hits.extend(data)

    print('Fetched page ' + str(page) + "; total hits " + str(len(hits)) + " (+" + str(len(data)) + "), total time " + str(round(end_time - start_overall_time)) + "ms (+" + str(round(end_time - start_time)) + "ms)")
    page += 1

print('Got ' + str(len(hits)) + ' hits')

if (len(hits) == 0):
    print('No data found')
    exit()

# Convert the "hits" array to a pandas DataFrame
df = pd.json_normalize(hits)

unused_fields = ['_index', '_type', '_id', '_score', '_source.stream', '_source._p', '_source.cluster',
                 '_source.region', '_source.service_version', '_source.az', '_source.ec2_instance_id',
                 '_source.private_ip', '_source.account_version', '_source.kubernetes.pod_name',
                 '_source.kubernetes.namespace_name', '_source.kubernetes.labels.app_kubernetes_io/component',
                 '_source.kubernetes.pod_id', '_source.kubernetes.labels.app_kubernetes_io/managed-by',
                 '_source.kubernetes.labels.app_kubernetes_io/name',
                 '_source.kubernetes.labels.app_kubernetes_io/part-of', '_source.kubernetes.labels.pod-template-hash',
                 '_source.kubernetes.annotations.checksum/config', '_source.kubernetes.annotations.prometheus_io/path',
                 '_source.kubernetes.annotations.prometheus_io/port',
                 '_source.kubernetes.annotations.prometheus_io/scrape', '_source.kubernetes.host',
                 '_source.kubernetes.docker_id', '_source.kubernetes.container_hash',
                 '_source.kubernetes.container_image', '_source.kubernetes.labels.app_kubernetes_io/instance',
                 '_source.kubernetes.container_name']
df = df.drop(columns=unused_fields)

df = df.rename(columns={'_source.@timestamp': 'timestamp', '_source.environment': 'environment',
                        '_source.time': 'account_local_time', '_source.service': 'service', '_source.loglevel': 'level',
                        '_source.msg': 'msg', '_source.account': 'account', '_source.ruid': 'ruid'})

df = df.reindex(['timestamp', 'account', 'service', 'level', 'msg', 'ruid', 'account_local_time', 'environment'],
                axis=1)

# Export the DataFrame to a CSV file
save_csv(df, 'results', 'search_results.csv', RAW)
