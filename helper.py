import os
import re

import pandas as pd


def ignoring_groups(regex):
    # Find all group patterns
    group_patterns = re.findall(r'\((.*?)\)', regex)

    # For each group pattern, replace it with a non-capturing version
    for group_pattern in group_patterns:
        if not group_pattern.startswith('?'):
            regex = regex.replace(f'({group_pattern})', f'(?:{group_pattern})')

    return regex


# permitted subdirectories
BASE = ''
RAW = 'raw'
INTERMEDIATE = 'intermediate'
FINAL = 'final'
PERMITTED_SUBDIRS = [BASE, RAW, INTERMEDIATE, FINAL]


def save_csv(df, results_dir, filename, subdir='intermediate'):
    if subdir not in PERMITTED_SUBDIRS:
        raise ValueError(f"subdir must be one of {PERMITTED_SUBDIRS}")

    target_dir = os.path.join(results_dir, subdir)

    if not os.path.exists(target_dir):
        os.makedirs(target_dir)

    df.to_csv(os.path.join(target_dir, filename), index=False)


def fill_int_blanks(df, field, fill_value='N/A'):
    df[field] = df[field].apply(lambda x: fill_value if pd.isnull(x) else int(x))
    return df
