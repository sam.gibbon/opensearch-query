from helper import ignoring_groups


# Required columns in dataframe: 'msg', 'ruid'
def failed_exports(df):
    # find rows where the export started but did not complete
    started_export_pattern = r'(\w+) - starting export of (\d+) records'

    started_mask = df['msg'].str.contains(ignoring_groups(started_export_pattern))

    completed_export_pattern = r'(\w) - completed export of (\d+) records'
    completed_mask = df['msg'].str.contains(ignoring_groups(completed_export_pattern))
    completed_ruids = df[completed_mask]['ruid'].unique()

    incomplete_export_mask = started_mask & ~df['ruid'].isin(completed_ruids)
    incomplete_export_df = df[incomplete_export_mask].copy()

    # extract the report type and number of exported records from the start message
    incomplete_export_df[['report_type', 'num_exported_records']] = incomplete_export_df['msg'].str.extract(
        started_export_pattern, expand=False)
    incomplete_export_df['num_exported_records'] = incomplete_export_df['num_exported_records'].astype(int)

    # clean up columns we don't need
    incomplete_export_df = incomplete_export_df.drop(columns=['msg'])

    return incomplete_export_df
