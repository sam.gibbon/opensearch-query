import pandas as pd
from helper import ignoring_groups, fill_int_blanks


def export_records_summary(df, failed_exports_df):
    # find rows where an export started
    started_export_pattern = r'(\w+) - starting export of (\d+) records'
    started_mask = df['msg'].str.contains(ignoring_groups(started_export_pattern))

    # extract the report type and number of records from the start message
    started_exports = df[started_mask].copy()
    started_exports[['report_type', 'num_exported_records']] = started_exports['msg'].str.extract(started_export_pattern, expand=False)

    # find successful exports by subtracting failed exports from started exports
    successful_exports = started_exports[~started_exports['ruid'].isin(failed_exports_df['ruid'])]

    # get maximum num_records for successful exports for each account and report_type
    max_successful_records_df = successful_exports.groupby(['account', 'report_type'])['num_exported_records'].agg(
        biggest_successful='max',
        num_successful_exports='count'
    )

    # get minimum num_records for failed exports for each account and report_type
    min_failed_records_df = failed_exports_df.groupby(['account', 'report_type'])['num_exported_records'].agg(
        smallest_failed='min',
        num_failed_exports='count'
    ).reset_index()

    # merge the two dataframes
    summary_df = pd.merge(max_successful_records_df, min_failed_records_df, on=['account', 'report_type'], how='outer')

    # fill in missing values
    summary_df = fill_int_blanks(summary_df, 'biggest_successful')
    summary_df = fill_int_blanks(summary_df, 'smallest_failed')
    summary_df = fill_int_blanks(summary_df, 'num_successful_exports', 0)
    summary_df = fill_int_blanks(summary_df, 'num_failed_exports', 0)

    return summary_df
