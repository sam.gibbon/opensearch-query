import pandas as pd
from helper import ignoring_groups


# Required columns in dataframe: 'msg', 'ruid'
def total_exports(df):
    # find rows where the export started
    started_export_pattern = r'(\w+) - starting export of (\d+) records'
    started_mask = df['msg'].str.contains(ignoring_groups(started_export_pattern))

    started_export_df = df[started_mask].copy()

    # extract the report type
    started_export_df[['report_type', 'num_exported_records']] = started_export_df['msg'].str.extract(started_export_pattern, expand=False)
    started_export_df['num_exported_records'] = pd.to_numeric(started_export_df['num_exported_records'])

    # group by account and report type
    started_export_df = started_export_df.groupby(['account', 'report_type']).agg(
        num_exports=('num_exported_records', 'size'),
        total_exported_records=('num_exported_records', 'sum'),
        avg_exported_records=('num_exported_records', 'mean'),
        min_exported_records=('num_exported_records', 'min'),
        max_exported_records=('num_exported_records', 'max')
    ).reset_index()
    started_export_df['avg_exported_records'] = started_export_df['avg_exported_records'].round(0).astype(int)

    return started_export_df
