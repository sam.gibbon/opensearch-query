# OpenSearch Query

A hacked together set of scripts for pulling logs from OpenSearch and analysing export data.

## Prerequisites

- python3
- libraries: requests, pandas, time, re

## Usage

Run `python3 search.py` to get logs from OpenSearch. Modify the query in the `search.py` file if need be. It will output a file called `search_results.csv`.

Run `python3 analyse.py` to analyse the data in `search_results.csv`. It will output the following files:
- `raw/failed_exports.csv` (plain data for each export failure)
- `intermediate/base_account_report_overview.csv` (basic statistics for each combination of account and report type)
- `intermediate/best_success_and_worst_failure.csv` (the largest successful export and smallest failed export for each combination of account and report type)
- `intermediate/failed_exports_by_account.csv` (`failed_records_summary` rolled up by account)
- `intermediate/failed_exports_by_report_type.csv` (`failed_records_summary` rolled up by report type)
- `intermediate/total_exports.csv` (all exports, whether success or fail, for each combination of account and report type)
- `intermediate/total_exports_by_account.csv` (`total_exports` rolled up by account)
- `intermediate/total_exports_by_report_type.csv` (`total_exports` rolled up by report type)
- `final/account_report_overview.csv` (combined overall report for each combination of account and report type)
- `final/account_report_overview_failures.csv` (combined overall report for each combination of account and report type with at least one failed export, sorted to put the worst offenders at the top)
- `final/overview_by_account.csv` (overall statistics by account)
- `final/overview_by_report_type.csv` (overall statistics by report type)
- `final/smallest_failed_by_report_type.csv` (smallest failed export for each report type, together with the account and call RUID associated with that export, including all records tied for smallest)
