import os
import pandas as pd
import analytics
from helper import save_csv, fill_int_blanks, RAW, INTERMEDIATE, FINAL

RESULTS_DIR = 'results'

results_df = pd.read_csv(os.path.join(RESULTS_DIR, RAW, 'search_results.csv'))
results_df = results_df.drop(columns=['environment', 'service', 'level', 'account_local_time'])

# get basic stats (incomplete exports, total exports)
base_failed_export_df = analytics.failed_exports(results_df)
save_csv(base_failed_export_df, RESULTS_DIR, 'failed_exports.csv', RAW)

failed_exports_df = base_failed_export_df.groupby(['account', 'report_type']).agg(
    num_failed_exports=('num_exported_records', 'size'),
    total_failed_exported_records=('num_exported_records', 'sum'),
    avg_failed_exported_records=('num_exported_records', 'mean'),
    min_failed_exported_records=('num_exported_records', 'min'),
    max_failed_exported_records=('num_exported_records', 'max')
).reset_index()
failed_exports_df['avg_failed_exported_records'] = failed_exports_df['avg_failed_exported_records'].round(0).astype(int)

total_export_df = analytics.total_exports(results_df)
save_csv(total_export_df, RESULTS_DIR, 'total_exports.csv', INTERMEDIATE)


# build some intermediate reports - failed and total exports by account and by report_type
def aggregate_failed_exports(grouped_df):
    aggregated_df = grouped_df.agg(
        num_failed_exports=('num_failed_exports', 'sum'),
        total_failed_exported_records=('total_failed_exported_records', 'sum'),
        smallest_failure=('min_failed_exported_records', 'min'),
        largest_failure=('max_failed_exported_records', 'max')
    ).reset_index()
    aggregated_df['avg_failed_exported_records'] = (aggregated_df['total_failed_exported_records'] / aggregated_df['num_failed_exports']).round(0).astype(int)
    aggregated_df.drop(columns=['total_failed_exported_records'], inplace=True)
    return aggregated_df


def aggregate_total_exports(grouped_df):
    aggregated_df = grouped_df.agg(
        num_exports=('num_exports', 'sum'),
        total_exported_records=('total_exported_records', 'sum'),
        smallest_export=('min_exported_records', 'min'),
        largest_export=('max_exported_records', 'max')
    ).reset_index()
    aggregated_df['avg_exported_records'] = (aggregated_df['total_exported_records'] / aggregated_df['num_exports']).round(0).astype(int)
    aggregated_df.drop(columns=['total_exported_records'], inplace=True)
    return aggregated_df


failed_exports_by_account_df = aggregate_failed_exports(failed_exports_df.groupby('account'))
failed_exports_by_report_type_df = aggregate_failed_exports(failed_exports_df.groupby('report_type'))
total_exports_by_account_df = aggregate_total_exports(total_export_df.groupby('account'))
total_exports_by_report_type_df = aggregate_total_exports(total_export_df.groupby('report_type'))

save_csv(failed_exports_by_account_df, RESULTS_DIR, 'failed_exports_by_account.csv', INTERMEDIATE)
save_csv(failed_exports_by_report_type_df, RESULTS_DIR, 'failed_exports_by_report_type.csv', INTERMEDIATE)
save_csv(total_exports_by_account_df, RESULTS_DIR, 'total_exports_by_account.csv', INTERMEDIATE)
save_csv(total_exports_by_report_type_df, RESULTS_DIR, 'total_exports_by_report_type.csv', INTERMEDIATE)


# compile overview by account report
def fill_extra_fields(df):
    df = fill_int_blanks(df, 'num_failed_exports', 0)
    df = fill_int_blanks(df, 'smallest_failure', 'N/A')
    df = fill_int_blanks(df, 'largest_failure', 'N/A')
    df = fill_int_blanks(df, 'avg_failed_exported_records', 'N/A')
    df = fill_int_blanks(df, 'num_exports', 0)
    df['percent_failed_exports'] = df.apply(lambda row: 0 if row['num_exports'] == 0 else round(row['num_failed_exports'] / row['num_exports'] * 100, 2), axis=1)
    return df


overview_by_account_df = pd.merge(failed_exports_by_account_df, total_exports_by_account_df, on='account', how='outer')
overview_by_account_df = fill_extra_fields(overview_by_account_df)
save_csv(overview_by_account_df, RESULTS_DIR, 'overview_by_account.csv', FINAL)

# compile overview by report_type report
overview_by_report_type_df = pd.merge(failed_exports_by_report_type_df, total_exports_by_report_type_df, on='report_type', how='outer')
overview_by_report_type_df = fill_extra_fields(overview_by_report_type_df)
save_csv(overview_by_report_type_df, RESULTS_DIR, 'overview_by_report_type.csv', FINAL)


# compile first level overview report
base_overview_df = pd.merge(failed_exports_df, total_export_df, on=['account', 'report_type'], how='outer')
base_overview_df = fill_int_blanks(base_overview_df, 'num_failed_exports', 0)
base_overview_df = fill_int_blanks(base_overview_df, 'num_exports', 0)
base_overview_df['percent_failed_exports'] = base_overview_df.apply(lambda row: 0 if row['num_exports'] == 0 else round(row['num_failed_exports'] / row['num_exports'] * 100, 2), axis=1)
save_csv(base_overview_df, RESULTS_DIR, 'base_account_report_overview.csv', INTERMEDIATE)

# calculate biggest successful export and smallest failed export for each account and report_type
export_records_summary_df = analytics.export_records_summary(results_df, base_failed_export_df)
save_csv(export_records_summary_df, RESULTS_DIR, 'best_success_and_worst_failure.csv', INTERMEDIATE)

# merge the last two reports into an overall summary
overview_df = pd.merge(export_records_summary_df, base_overview_df, on=['account', 'report_type'], how='left')
# sanity check - verify that the merge has the same num_failed_exports figure from both sides
assert overview_df['num_failed_exports_x'].equals(overview_df['num_failed_exports_y'])

# cleanup columns before saving final report
overview_df = overview_df.drop(columns=['num_failed_exports_x'])
overview_df = overview_df.rename(columns={'num_failed_exports_y': 'num_failed_exports'})
overview_df = overview_df[[
    'account',
    'report_type',
    'num_successful_exports',
    'num_failed_exports',
    'num_exports',
    'percent_failed_exports',
    'biggest_successful',
    'smallest_failed'
]]
save_csv(overview_df, RESULTS_DIR, 'account_report_overview.csv', FINAL)

# produce filtered version of previous report showing only combinations that resulted in failed exports
failed_exports_overview_df = overview_df[overview_df['num_failed_exports'] > 0].copy()
failed_exports_overview_df = failed_exports_overview_df.sort_values(by=['percent_failed_exports', 'num_failed_exports'], ascending=False)
save_csv(failed_exports_overview_df, RESULTS_DIR, 'account_report_overview_failures.csv', FINAL)


# produce report of report_types, their smallest failed export and the account/ruid associated with that export
smallest_failed_by_report_type_df = (failed_exports_overview_df.groupby('report_type')['smallest_failed']
                                     .min().reset_index()
                                     .sort_values(by='smallest_failed', ascending=True))
smallest_failed_by_report_type_df = pd.merge(smallest_failed_by_report_type_df, failed_exports_overview_df[['report_type', 'account', 'smallest_failed']], on=['report_type', 'smallest_failed'], how='left')
smallest_failed_by_report_type_df = pd.merge(smallest_failed_by_report_type_df, base_failed_export_df[['ruid', 'report_type', 'account']], on=['report_type', 'account'], how='left')
save_csv(smallest_failed_by_report_type_df, RESULTS_DIR, 'smallest_failed_by_report_type.csv', FINAL)


# print some overall statistics
total_successful_exports = export_records_summary_df['num_successful_exports'].sum()
total_failed_exports = export_records_summary_df['num_failed_exports'].sum()
total_exports = total_successful_exports + total_failed_exports

print(f"Total successful exports: {total_successful_exports} ({round(total_successful_exports / total_exports * 100, 2)}%)")
print(f"Total failed exports: {total_failed_exports} ({round(total_failed_exports / total_exports * 100, 2)}%)")
print(f"Number of accounts with failed exports: {len(failed_exports_by_account_df)} / {len(total_exports_by_account_df)}")
print(f"Number of report types with failed exports: {len(failed_exports_by_report_type_df)} / {len(total_exports_by_report_type_df)}")
